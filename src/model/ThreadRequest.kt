package com.r4sh33d.model

import com.google.gson.annotations.SerializedName

data class ThreadRequest(
    @SerializedName("tweet_url")
    val tweetUrl: String,
    @SerializedName("user_twitter_secrete")
    val userSecrete: String?,
    @SerializedName("user_twitter_token")
    val userToken: String?
)