package com.r4sh33d.model

import com.google.gson.annotations.SerializedName

class SuccessResponse<T>(
    @SerializedName("data")
    val data: T,
    @SerializedName("status")
    val status: String = "success"
)

data class ErrorResponse<T>(
    @SerializedName("error")
    val error: T,
    @SerializedName("status")
    val status: String = "error"
)

data class ThreaderError(
    @SerializedName("message")
    val message: String,
    @SerializedName("code")
    val code: String,
    @SerializedName("type")
    val type: String = ""
)