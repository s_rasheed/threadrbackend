package com.r4sh33d

import com.r4sh33d.twitterapi.getThreadRepository
import com.r4sh33d.twitterapi.oauth.OauthKeys
import io.ktor.application.*
import io.ktor.auth.Principal
import io.ktor.auth.authentication
import io.ktor.features.*
import io.ktor.gson.gson
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.request.header
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.routing
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.error

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    val config = environment.config
    val production = config.config("service").property("environment").getString() == "production"
    val localProperties = getLocalProperties() ?: throw Throwable("Please specify 'local.properties' file")

    val threaderApiSecrete = getThreaderToken(localProperties)

    val oauthKeys = OauthKeys(
        getTwitterAppConsumerKey(localProperties),
        getTwitterAppConsumerSecrete(localProperties)
    )

    val threadRepository = getThreadRepository(oauthKeys, log)

    println("Starting the server")

    // initializeFirebaseAdminSdk()

    if (!production) {
        install(CallLogging)
    }

    install(Locations)
    install(DefaultHeaders)
    install(ContentNegotiation) { gson { setPrettyPrinting() } }

    install(StatusPages) {

        exception<NotAThread> {
            call.respond(HttpStatusCode.NotFound, Errors.NOT_A_THREAD_ERROR)
        }

        exception<UnableToRetrieveTweet> {
            call.respond(HttpStatusCode.NotFound, Errors.UNABLE_TO_RETRIEVE_TWEET_ERROR)
        }

        exception<BadRequest> {
            call.respond(HttpStatusCode.BadRequest, Errors.BAD_REQUEST_ERROR)
        }

        exception<Unauthorized> {
            call.respond(HttpStatusCode.Unauthorized, Errors.UNAUTHORIZED_ERROR)
        }

        exception<NotFound> {
            call.respond(HttpStatusCode.NotFound, Errors.NOT_FOUND_ERROR)
        }

        exception<Throwable> { cause ->
            environment.log.error(cause)
            call.respond(HttpStatusCode.InternalServerError, Errors.INTERNAL_SERVER_ERROR)
        }

        exception<InvalidSecrete> {
            call.respond(HttpStatusCode.Forbidden, Errors.INVALID_SECRETE_ERROR)
        }
    }

    install(CORS) {
        anyHost()
        header(HttpHeaders.Authorization)
        allowCredentials = true
        listOf(HttpMethod.Put, HttpMethod.Delete, HttpMethod.Options).forEach { method(it) }
    }

    routing {
        authenticate()
        api(threadRepository, threaderApiSecrete)
    }
}

private fun Route.authenticate() {
    val bearer = "Bearer "
    intercept(ApplicationCallPipeline.Features) {
        val authorization = call.request.header(HttpHeaders.Authorization) ?: return@intercept
        if (!authorization.startsWith(bearer)) return@intercept
        val token = authorization.removePrefix(bearer).trim()
        call.authentication.principal(ThreaderPrincipal(token))
    }
}

internal class ThreaderPrincipal(val token: String) : Principal