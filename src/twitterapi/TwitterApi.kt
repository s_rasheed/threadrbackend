package com.r4sh33d.twitterapi

import com.google.gson.Gson
import com.r4sh33d.twitterapi.models.OAuth2Token
import com.r4sh33d.twitterapi.models.Tweet
import com.r4sh33d.twitterapi.oauth.Oauth2BasicTokenInterceptor
import com.r4sh33d.twitterapi.oauth.Oauth2SigningHelper
import com.r4sh33d.twitterapi.oauth.OauthKeys
import com.r4sh33d.twitterapi.oauth.OauthSigningInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.slf4j.Logger
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

//TODO manage deps with Dagger2? (or any other SL/DI framework?), especially the API services.

class TwitterThreadApi constructor(
    private val twitterApiService: TwitterApiService
) {

    suspend fun getUserTimeline(
        screenName: String, count: Int, maxId: Long?, sinceId: Long?,
        userAccessToken: String?, userAccessSecrete: String?
    ): Result<List<Tweet>> {
        return try {
            getAPIResult(
                twitterApiService.getUserTimeline(
                    screenName,
                    count,
                    maxId,
                    sinceId,
                    userAccessToken,
                    userAccessSecrete
                )
            )
        } catch (e: Exception) {
            Result.Error(GENERIC_ERROR_CODE, e.message ?: GENERIC_ERROR_MESSAGE)
        }
    }

    suspend fun getTweet(id: String, userAccessToken: String?, userAccessSecrete: String?): Result<Tweet> {
        return try {
            getAPIResult(twitterApiService.getTweet(id, userAccessToken, userAccessSecrete))
        } catch (e: Exception) {
            Result.Error(GENERIC_ERROR_CODE, e.message ?: GENERIC_ERROR_MESSAGE)
        }
    }
}

interface TwitterApiService {

    companion object {
        private const val ENDPOINT = "https://api.twitter.com/"
        const val HEADER_USER_ACCESS_SECRETE = "USER_ACCESS_SECRETE"
        const val HEADER_USER_ACCESS_TOKEN = "USER_ACCESS_TOKEN"

        @Volatile
        private var instance: TwitterApiService? = null

        fun getInstance(oauthKeys: OauthKeys): TwitterApiService =
            instance ?: synchronized(this) {
                instance ?: buildApiService(oauthKeys).also { instance = it }
            }

        private fun buildApiService(oauthKeys: OauthKeys) =
            Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .client(getOkHttpClient(oauthKeys))
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .build()
                .create(TwitterApiService::class.java)

        private fun getOkHttpClient(oauthKeys: OauthKeys): OkHttpClient =
            OkHttpClient.Builder()
                .addInterceptor(OauthSigningInterceptor(oauthKeys))
                .addInterceptor(getLoggingInterceptor()).build()
    }

    @GET("/1.1/statuses/show.json")
    suspend fun getTweet(
        @Query("id") id: String,
        @Header(HEADER_USER_ACCESS_TOKEN) userAccessToken: String?,
        @Header(HEADER_USER_ACCESS_SECRETE) userAccessSecrete: String?,
        @Query("tweet_mode") tweetMode: String = "extended"

    ): Response<Tweet>

    @GET("/1.1/statuses/user_timeline.json")
    suspend fun getUserTimeline(
        @Query("screen_name") screeName: String, @Query("count") count: Int,
        @Query("max_id") maxId: Long?,
        @Query("since_id") sinceId: Long?,
        @Header(HEADER_USER_ACCESS_TOKEN) userAccessToken: String?,
        @Header(HEADER_USER_ACCESS_SECRETE) userAccessSecrete: String?,
        @Query("tweet_mode") tweetMode: String = "extended"
    ): Response<List<Tweet>>
}

interface TwitterApiAuthService {

    companion object {
        private const val ENDPOINT = "https://api.twitter.com/"

        @Volatile
        private var instance: TwitterApiAuthService? = null

        fun getInstance(oauthKeys: OauthKeys): TwitterApiAuthService =
            instance ?: synchronized(this) {
                instance ?: buildApiService(oauthKeys).also { instance = it }
            }

        private fun buildApiService(oauthKeys: OauthKeys) =
            Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .client(getOkHttpClient(oauthKeys))
                .build().create(TwitterApiAuthService::class.java)

        private fun getOkHttpClient(oauthKeys: OauthKeys): OkHttpClient =
            OkHttpClient.Builder().addInterceptor(Oauth2BasicTokenInterceptor(oauthKeys))
                .addInterceptor(getLoggingInterceptor()).build()
    }

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=UTF-8")
    @FormUrlEncoded
    @POST("/oauth2/token")
    fun getAppAuthToken(
        @Field(Oauth2SigningHelper.PARAM_GRANT_TYPE) grantType: String
    ): Call<OAuth2Token>
}

fun getLoggingInterceptor(): HttpLoggingInterceptor =
    HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

fun getThreadRepository(oauthKeys: OauthKeys, logger: Logger) =
    TwitterThreadRepository(TwitterThreadApi(TwitterApiService.getInstance(oauthKeys)), logger)
