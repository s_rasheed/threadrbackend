package com.r4sh33d.twitterapi

import retrofit2.Response

const val GENERIC_ERROR_MESSAGE = "We are unable to proceed due to network failure. Please try again"
const val GENERIC_ERROR_CODE = "-1"

fun <T : Any> getAPIResult(response: Response<T>): Result<T> {
    if (response.isSuccessful) {
        val body = response.body()
        if (body != null) {
            return Result.Success(body)
        }
    }
    // Fallback to regular status code and message
    return Result.Error("${response.code()}", response.message())
}