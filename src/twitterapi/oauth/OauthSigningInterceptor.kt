package com.r4sh33d.twitterapi.oauth

import com.r4sh33d.twitterapi.TwitterApiService
import com.r4sh33d.twitterapi.oauth.Oauth1SigningHelper.signRequestWithOauth1
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.util.*

class OauthSigningInterceptor(
    private var oauthKeys: OauthKeys,
    private val nonce: String = UUID.randomUUID().toString(),
    private val timestamp: Long = System.currentTimeMillis() / 1000L
) : Interceptor {

    private val oauth2SigningHelper = Oauth2SigningHelper(oauthKeys)

    companion object {
        const val RATE_LIMIT_EXCEEDED_CODE = 429
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val userAccessToken = request.header(TwitterApiService.HEADER_USER_ACCESS_TOKEN)
        val userAccessSecret = request.header(TwitterApiService.HEADER_USER_ACCESS_SECRETE)

        //check if we can verify with user creds
        if (userAccessToken != null && userAccessSecret != null) {
            val keys = oauthKeys.apply {
                accessToken = userAccessToken
                accessSecret = userAccessSecret
            }
            //Try to use user-scoped oauth
            val response = chain.proceed(signRequestWithOauth1(request, keys, nonce, timestamp))
            //Check if we hit the rate limit, then retry with oauth2, else, just return the response like that
            if (!response.isSuccessful && response.code == RATE_LIMIT_EXCEEDED_CODE) {
                request.newBuilder().removeHeader(Oauth2SigningHelper.AUTH_HEADER_KEY)
            } else {
               // println("About to return the response")
                return response
            }
        }
        return chain.proceed(oauth2SigningHelper.signRequestWithOauth2(request))
    }
}
