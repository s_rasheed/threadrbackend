package com.r4sh33d.twitterapi.oauth

import com.r4sh33d.twitterapi.*
import com.r4sh33d.twitterapi.models.OAuth2Token
import okhttp3.Request

class Oauth2SigningHelper(private val oauthKeys: OauthKeys) {

    companion object {
        const val AUTH_HEADER_KEY = "Authorization"
        const val PARAM_GRANT_TYPE = "grant_type"
        const val GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials"
    }

    private val accessToken = getToken()

    fun signRequestWithOauth2(request: Request): Request {
        return request.newBuilder()
            .addHeader(AUTH_HEADER_KEY, "Bearer $accessToken")
            .build()
    }

    fun getToken(): String? {
        return when (val result = getAccessTokenFromNetwork()) {
            is Result.Success -> result.data.accessToken
            is Result.Error -> null
        }
    }

    fun getAccessTokenFromNetwork(): Result<OAuth2Token> {
        return try {
            getAPIResult(
                TwitterApiAuthService.getInstance(oauthKeys)
                    .getAppAuthToken(GRANT_TYPE_CLIENT_CREDENTIALS).execute()
            )
        } catch (e: Exception) {
            println("Fetching token from network Error:\n${e}")
            e.printStackTrace()
            Result.Error(GENERIC_ERROR_CODE, e.message ?: GENERIC_ERROR_MESSAGE)
        }
    }
}