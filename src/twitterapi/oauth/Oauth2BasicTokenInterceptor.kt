package com.r4sh33d.twitterapi.oauth

import okhttp3.Interceptor
import okhttp3.Response
import okio.ByteString.Companion.encodeUtf8

class Oauth2BasicTokenInterceptor(oauthKeys: OauthKeys) : Interceptor {

    companion object {
        private const val AUTH_HEADER_KEY = "Authorization"
    }

    private val headerString =
        UrlUtils.percentEncode(oauthKeys.consumerKey) + ":" + UrlUtils.percentEncode(oauthKeys.consumerSecret)

    override fun intercept(chain: Interceptor.Chain): Response {
        val authenticatedRequest = chain.request()
            .newBuilder()
            .addHeader(AUTH_HEADER_KEY, "Basic ${headerString.encodeUtf8().base64()}")
            .build()
        return chain.proceed(authenticatedRequest)
    }
}