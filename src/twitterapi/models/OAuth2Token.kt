package com.r4sh33d.twitterapi.models

import com.google.gson.annotations.SerializedName

data class OAuth2Token(
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("access_token")
    val accessToken: String
)