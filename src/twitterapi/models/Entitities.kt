package com.r4sh33d.twitterapi.models

import com.google.gson.annotations.SerializedName
import com.r4sh33d.twitterapi.models.Url
import com.r4sh33d.twitterapi.models.UserMention

data class Entities(
    @SerializedName("urls")
    val urls: List<Url>,
    @SerializedName("user_mentions")
    val userMentions: List<UserMention>
)

data class ExtendedEntities(
    @SerializedName("media")
    val media: List<Media>
)

data class Media(
    @SerializedName("id")
    val id: Long,
    @SerializedName("id_str")
    val idStr: String,
    @SerializedName("indices")
    val indices: List<Int>,
    @SerializedName("media_url")
    val mediaUrl: String,
    @SerializedName("media_url_https")
    val mediaUrlHttps: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("video_info")
    val videoInfo: VideoInfo?
)

data class Url(
    @SerializedName("display_url")
    val displayUrl: String,
    @SerializedName("expanded_url")
    val expandedUrl: String,
    @SerializedName("indices")
    val indices: List<Int>,
    @SerializedName("url")
    val url: String
)

data class UserMention(
    @SerializedName("id")
    val id: Long,
    @SerializedName("id_str")
    val idStr: String,
    @SerializedName("indices")
    val indices: List<Int>,
    @SerializedName("name")
    val name: String,
    @SerializedName("screen_name")
    val screenName: String
)

data class VideoInfo(
    @SerializedName("aspect_ratio")
    val aspectRatio: List<Int>,
    @SerializedName("duration_millis")
    val durationMillis: Int,
    @SerializedName("variants")
    val variants: List<Variant>
)

data class Variant(
    @SerializedName("bitrate")
    val bitrate: Int?,
    @SerializedName("content_type")
    val contentType: String,
    @SerializedName("url")
    val url: String
)