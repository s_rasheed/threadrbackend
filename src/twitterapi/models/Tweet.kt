package com.r4sh33d.twitterapi.models

import com.google.gson.annotations.SerializedName

@Suppress( "SelfReferenceConstructorParameter")
data class Tweet(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("id_str")
    val idStr: String,
    @SerializedName("in_reply_to_screen_name")
    val inReplyToScreenName: String?,
    @SerializedName("in_reply_to_status_id")
    val inReplyToStatusId: Long?,
    @SerializedName("in_reply_to_status_id_str")
    val inReplyToStatusIdStr: String?,
    @SerializedName("in_reply_to_user_id")
    val inReplyToUserId: Long?,
    @SerializedName("in_reply_to_user_id_str")
    val inReplyToUserIdStr: String?,
    @SerializedName("source")
    val source: String?,
    @SerializedName("full_text")
    val text: String,
    @SerializedName("truncated")
    val truncated: Boolean,
    @SerializedName("user")
    val user: User,
    @SerializedName("entities")
    val entities: Entities,
    @SerializedName("display_text_range")
    val displayTextRange: List<Int>,
    @SerializedName("quoted_status")
    val quotedStatus: Tweet?,
    @SerializedName("extended_entities")
    val extendedEntities: ExtendedEntities?,
    @SerializedName("quoted_status_permalink")
    val quotedStatusPermalink: QuotedStatusPermalink?
)

data class QuotedStatusPermalink(
    @SerializedName("display")
    val display: String,
    @SerializedName("expanded")
    val expanded: String,
    @SerializedName("url")
    val url: String
)

data class User(
    @SerializedName("id")
    val id: Long,
    @SerializedName("id_str")
    val idStr: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("screen_name")
    val screenName: String,
    @SerializedName("profile_image_url_https")
    val profileImageUrl: String
)