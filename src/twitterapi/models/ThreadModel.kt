package com.r4sh33d.twitterapi.models

import com.google.gson.annotations.SerializedName

data class Thread(
    @SerializedName("thread")
    val tweets: List<Tweet>,
    @SerializedName("summary")
    val summary: String,
    @SerializedName("preview_image_url")
    val previewImageUrl: String?,
    @SerializedName("signature")
    val signature: String,
    @SerializedName("created_at")
    val createdAt: String = System.currentTimeMillis().toString()
)
