package com.r4sh33d.twitterapi

import com.r4sh33d.NotAThread
import com.r4sh33d.UnableToRetrieveTweet
import com.r4sh33d.model.ThreadRequest
import com.r4sh33d.twitterapi.models.Tweet
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import org.slf4j.Logger
import com.r4sh33d.twitterapi.models.Thread
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.ArrayList

typealias ThreadModel = Thread //To avoid auto-complete conflicts with java.lang.Thread

class TwitterThreadRepository(private val twitterThreadApi: TwitterThreadApi, private val logger: Logger) {

    suspend fun getTwitterThread(threadRequest: ThreadRequest): ThreadModel {
        val tweetId = getIdFromTweetUrl(threadRequest.tweetUrl) ?: throw UnableToRetrieveTweet()
        return getThread(
            tweetId,
            threadRequest
        )
    }

    private fun getIdFromTweetUrl(url: String) =
        url.split("/").lastOrNull()?.split("?")?.firstOrNull()

    private suspend fun getThread(tweetId: String, request: ThreadRequest) =
        coroutineScope {
            val tweets = ArrayList<Tweet>()
            when (val result = twitterThreadApi.getTweet(tweetId, request.userToken, request.userSecrete)) {
                is Result.Success -> {
                    val replies = async { getTweetReplies(result.data, request) }
                    val parents = async { getTweetParents(result.data, request) }
                    tweets.apply {
                        addAll(parents.await())
                        add(result.data)
                        addAll(replies.await())
                    }
                    if (tweets.size <= 1) throw NotAThread()
                }
                is Result.Error -> {
                    logError(result.errorMessage)
                    throw UnableToRetrieveTweet()
                }
            }
            //If we reach here, we have a valid thread
            prepareThread(tweets)
        }

    private suspend fun getTweetReplies(baseTweet: Tweet, request: ThreadRequest): List<Tweet> {
        val allTweetsSinceBase = ArrayList<Tweet>()
        var currentBaseTweet = baseTweet
        val tweetList = ArrayList<Tweet>()
        var maxId: Long? = null
        loop@ while (true) {
            when (val result = twitterThreadApi.getUserTimeline(
                baseTweet.user.screenName, 200, maxId, baseTweet.id, request.userToken, request.userSecrete
            )) {
                is Result.Success -> {
                    if (result.data.isEmpty()) break@loop
                    allTweetsSinceBase.addAll(result.data)
                    result.data.lastOrNull()?.id?.let { maxId = it - 1 }
                }
                is Result.Error -> {
                    logError(result.errorMessage)
                    break@loop
                }
            }
        }
        for (i in allTweetsSinceBase.size - 1 downTo 0) {
            val tweet = allTweetsSinceBase[i]
            if (tweet.inReplyToStatusIdStr.equals(currentBaseTweet.idStr)) {
                tweetList.add(tweet)
                currentBaseTweet = tweet
            }
        }
        return tweetList
    }

    private suspend fun getTweetParents(childTweet: Tweet, request: ThreadRequest): List<Tweet> {
        var count = 0
        val tweetList = LinkedList<Tweet>()
        var currentChildTweet = childTweet
        loop@ while (true) {
            when (val result = twitterThreadApi.getUserTimeline(
                childTweet.user.screenName, 200, currentChildTweet.id, null, request.userToken, request.userSecrete
            )) {
                is Result.Success -> {
                    count++
                    if (result.data.isEmpty()) {
                        return tweetList
                    }
                    for (tweet in result.data) {
                        if (currentChildTweet.inReplyToStatusIdStr == tweet.idStr) {
                            tweetList.addFirst(tweet)
                            currentChildTweet = tweet
                        }
                    }
                    if (currentChildTweet.inReplyToStatusIdStr == null) {
                        // We've seen the base tweet
                        return tweetList
                    }
                }
                is Result.Error -> {
                    logError(result.errorMessage)
                    return tweetList
                }
            }
        }
    }

    private fun prepareThread(tweets: ArrayList<Tweet>): ThreadModel {
        var previewImageUrl: String? = null
        val maxCharacterCount = 150
        val summary = StringBuilder()
        for (tweet in tweets) {
            if (summary.length < maxCharacterCount) summary.append(tweet.text)
            val imageUrl =  tweet.extendedEntities?.media?.firstOrNull()?.mediaUrlHttps
            if (previewImageUrl == null && imageUrl != null) previewImageUrl = imageUrl
            if (summary.length >= maxCharacterCount && previewImageUrl != null) break
        }
        val signature = "${tweets.first().idStr}::${tweets.last().idStr}"
        return ThreadModel(
            tweets, summary.toString().replace(REMOVE_LINKS_REGEX, ""),
            previewImageUrl, signature
        )
    }

    private fun logThread(thread: ThreadModel) {
        logMessage("About to print the motherFucking Thread:\n[")
        thread.tweets.forEach {
            logMessage(it.text)
        }
        logMessage("]")
    }

    private fun logMessage(message: String) = println(message)
    private fun logError(error: String) = logger.error(error)


    companion object {
        val REMOVE_LINKS_REGEX = Regex("https?://\\S+\\s?")
    }
}
