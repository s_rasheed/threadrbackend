package com.r4sh33d

import com.r4sh33d.model.ErrorResponse
import com.r4sh33d.model.ThreaderError

object Errors {

    val NOT_A_THREAD_ERROR = ErrorResponse(
        ThreaderError(
            "The selected tweet is not part of a thread",
            ErrorCodes.NOT_A_THREAD
        )
    )

    val UNABLE_TO_RETRIEVE_TWEET_ERROR = ErrorResponse(
        ThreaderError("We are unable to retrieve the tweet", ErrorCodes.UNABLE_TO_RETRIEVE_TWEET)
    )

    val BAD_REQUEST_ERROR = ErrorResponse(
        (ThreaderError(
            "Bad request, please check your request again",
            ErrorCodes.BAD_REQUEST
        ))
    )

    val UNAUTHORIZED_ERROR = ErrorResponse(
        (ThreaderError(
            "Can not authorize you, Missing/Invalid API secrete",
            ErrorCodes.UNAUTHORIZED
        ))
    )

    val INVALID_SECRETE_ERROR = ErrorResponse(
        (ThreaderError(FORBIDDEN_ERROR_MESSAGE, ErrorCodes.INVALID_SECRETE))
    )

    val NOT_FOUND_ERROR = ErrorResponse(
        (ThreaderError("Resource not found", ErrorCodes.NOT_FOUND))
    )

    val INTERNAL_SERVER_ERROR = ErrorResponse(
        (ThreaderError("Error, this is not your fault, we'll look in to it. ", ErrorCodes.INTERNAL_SERVER))
    )

}


object ErrorCodes {
    const val NOT_A_THREAD = "20"
    const val UNABLE_TO_RETRIEVE_TWEET = "30"
    const val BAD_REQUEST = "40"
    const val UNAUTHORIZED = "50"
    const val INVALID_SECRETE = "60"
    const val NOT_FOUND = "70"
    const val INTERNAL_SERVER = "80"
}


class NotAThread : Throwable()
class BadRequest : Throwable()
class UnableToRetrieveTweet : Throwable()
class Unauthorized : Throwable()
class NotFound : Throwable()
class InvalidSecrete : Throwable()


class MissingProperties(propName: String) :
    Throwable("Please specify '$propName' in your 'local.properties' file in the project root directory")


const val FORBIDDEN_ERROR_MESSAGE = """
   - - -  Forbidden  - - - 
   
          .-''''''-.
        .' _      _ '.
       /   O      O   \\
      :                :
      |        ^       |
      :       __       :
       \  .-"`  `"-.  /
        '.          .'
          '-......-'
     YOU SHOULDN'T BE HERE
"""
