package com.r4sh33d

import com.r4sh33d.model.SuccessResponse
import com.r4sh33d.model.ThreadRequest
import com.r4sh33d.twitterapi.TwitterThreadRepository
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.auth.principal
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.post
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Routing

@KtorExperimentalLocationsAPI
internal fun Routing.api(threadRepository: TwitterThreadRepository, clientSecrete: String) {
    apiThread(threadRepository, clientSecrete)
}

@KtorExperimentalLocationsAPI
private fun Routing.apiThread(threadRepository: TwitterThreadRepository, clientSecrete: String) {
    post<ThreadLocation> {
        call.validateSecret(clientSecrete)
        val threadRequest = call.receive<ThreadRequest>()
        call.respond(SuccessResponse(threadRepository.getTwitterThread(threadRequest)))
    }
}

private fun ApplicationCall.validateSecret(clientSecrete: String) {
    val principal = principal<ThreaderPrincipal>()
    if (principal?.token != clientSecrete) throw Unauthorized()
}
