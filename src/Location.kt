package com.r4sh33d

import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

/**
 * Location for a thread
 */

@KtorExperimentalLocationsAPI
@Location("/thread")
class ThreadLocation
