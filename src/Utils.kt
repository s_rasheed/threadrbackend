package com.r4sh33d

import java.io.FileInputStream
import java.io.IOException
import java.util.*

internal fun getLocalProperties() = getProperties("local.properties")

internal fun getProperties(fileName: String): Properties? {
    return try {
        FileInputStream(fileName).use { input ->
            Properties().apply { load(input) }
        }
    } catch (ex: IOException) {
        ex.printStackTrace()
        null
    }
}

fun getTwitterAppConsumerKey(properties: Properties) =
    properties.getProperty("twitter-app-consumer-key") ?: throw MissingProperties("twitter-app-api-secrete")

fun getTwitterAppConsumerSecrete(properties: Properties) =
    properties.getProperty("twitter-app-consumer-secrete") ?: throw MissingProperties("twitter-app-consumer-secrete")


fun getTwitterApiToken(properties: Properties) =
    properties.getProperty("twitter-app-api-token") ?: throw MissingProperties("twitter-api-token")

fun getThreaderToken(properties: Properties) =
    properties.getProperty("threader-api-token") ?: throw MissingProperties("threader-api-token")
